var namespaces_dup =
[
    [ "BNO055", null, [
      [ "BNO055", "classBNO055_1_1BNO055.html", "classBNO055_1_1BNO055" ]
    ] ],
    [ "BNO055_1", null, [
      [ "BNO055", "classBNO055__1_1_1BNO055.html", "classBNO055__1_1_1BNO055" ]
    ] ],
    [ "closedloop", "namespaceclosedloop.html", "namespaceclosedloop" ],
    [ "closedloop1", "namespaceclosedloop1.html", "namespaceclosedloop1" ],
    [ "DRV8847", "namespaceDRV8847.html", "namespaceDRV8847" ],
    [ "encoder", "namespaceencoder.html", "namespaceencoder" ],
    [ "Lab0x01", "namespaceLab0x01.html", [
      [ "onButtonPressFCN", "namespaceLab0x01.html#a30a5cf8034929397cada3568b302d96a", null ],
      [ "reset_timer", "namespaceLab0x01.html#ac60cf12ea29eb4c53b0a55d65db78cf0", null ],
      [ "update_sqw", "namespaceLab0x01.html#a1096e462a48db59bab9e109414d88c46", null ],
      [ "update_stw", "namespaceLab0x01.html#a18fa12aa8ad39e909fac50b79694ba6e", null ],
      [ "update_sw", "namespaceLab0x01.html#aa0a49bcd116107f1665d68c959bf8cf6", null ],
      [ "update_timer", "namespaceLab0x01.html#af64d1c21b3c11168bc38682464776560", null ],
      [ "button_pressed", "namespaceLab0x01.html#a788650c0ce5e1492a9b8e78f823b3e75", null ],
      [ "ButtonInt", "namespaceLab0x01.html#a7b9c33e3119acd6ca7b401a1fb51bc9f", null ],
      [ "pinA5", "namespaceLab0x01.html#a85ea292058f905478f79d7b582f8cac8", null ],
      [ "pinC13", "namespaceLab0x01.html#ab7350683e4e73629aaa40f90ce9b45d9", null ],
      [ "runs", "namespaceLab0x01.html#a0cd01b949f895ba36e5a15d76c57071a", null ],
      [ "start", "namespaceLab0x01.html#a5c3a3f54bf2fc547f341091f8676a385", null ],
      [ "state", "namespaceLab0x01.html#af49dfbadfaec711466de0f806232bf66", null ],
      [ "t2ch1", "namespaceLab0x01.html#a3b58c14328a9821ec59cfb491f02bdf9", null ],
      [ "tim2", "namespaceLab0x01.html#a778761870b1d6720e614c945eaa72b9f", null ]
    ] ],
    [ "Lab0x02", "namespaceLab0x02.html", [
      [ "main", "namespaceLab0x02.html#a60f19abc059e61542a66f48c89127d2a", null ]
    ] ],
    [ "main", "namespacemain.html", [
      [ "main", "namespacemain.html#af613cea4cba4fb7de8e40896b3368945", null ]
    ] ],
    [ "main1", "namespacemain1.html", [
      [ "main", "namespacemain1.html#a5b1d5860701e90b9937b1726ee8642d1", null ]
    ] ],
    [ "motor_driver1", "namespacemotor__driver1.html", "namespacemotor__driver1" ],
    [ "shares", "namespaceshares.html", "namespaceshares" ],
    [ "task_closedloop", "namespacetask__closedloop.html", "namespacetask__closedloop" ],
    [ "task_closedloop1", "namespacetask__closedloop1.html", "namespacetask__closedloop1" ],
    [ "task_encoder", "namespacetask__encoder.html", "namespacetask__encoder" ],
    [ "task_imu1", "namespacetask__imu1.html", "namespacetask__imu1" ],
    [ "task_motor", "namespacetask__motor.html", "namespacetask__motor" ],
    [ "task_motor1", "namespacetask__motor1.html", "namespacetask__motor1" ],
    [ "task_touch1", "namespacetask__touch1.html", "namespacetask__touch1" ],
    [ "task_user", "namespacetask__user.html", "namespacetask__user" ],
    [ "task_user1", "namespacetask__user1.html", "namespacetask__user1" ],
    [ "touch1", "namespacetouch1.html", "namespacetouch1" ]
];