var indexSectionsWithContent =
{
  0: "_bcdefghiklmnopqrstuvwxyz",
  1: "bcdempqst",
  2: "cdelmst",
  3: "m",
  4: "_cdefgmnoprstuwxyz",
  5: "bcdefhiklmnprstvwxyz",
  6: "m"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "variables",
  6: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Files",
  4: "Functions",
  5: "Variables",
  6: "Pages"
};

