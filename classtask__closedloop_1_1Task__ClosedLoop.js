var classtask__closedloop_1_1Task__ClosedLoop =
[
    [ "__init__", "classtask__closedloop_1_1Task__ClosedLoop.html#a95582c085292061a7d73948225014c8d", null ],
    [ "run", "classtask__closedloop_1_1Task__ClosedLoop.html#a1de520b2a313d0e06d6795beb9cd24fd", null ],
    [ "transition_to", "classtask__closedloop_1_1Task__ClosedLoop.html#aaa438036b5e495e7ca73204c5954cec8", null ],
    [ "CL_object", "classtask__closedloop_1_1Task__ClosedLoop.html#aa2723f83c6846d3694bf97d38b695e57", null ],
    [ "dbg", "classtask__closedloop_1_1Task__ClosedLoop.html#a3ff6e343dffeea7ec3ccd527c104d23a", null ],
    [ "enc_speed", "classtask__closedloop_1_1Task__ClosedLoop.html#add57a0aca118ddd5e6e2662ac30ac2b0", null ],
    [ "kp", "classtask__closedloop_1_1Task__ClosedLoop.html#a7d5bd7594d2944691e64a4de512494bd", null ],
    [ "kp_flag", "classtask__closedloop_1_1Task__ClosedLoop.html#a586626a45cbae415226f77d302f1ec9c", null ],
    [ "L", "classtask__closedloop_1_1Task__ClosedLoop.html#a129d398eb23113d80231728a973c74f5", null ],
    [ "L_flag", "classtask__closedloop_1_1Task__ClosedLoop.html#a9fd5774b092fcd7384f6d4c4d07fe6a3", null ],
    [ "L_share", "classtask__closedloop_1_1Task__ClosedLoop.html#a788a1ee324db16ea11d6d60bfd2c4150", null ],
    [ "name", "classtask__closedloop_1_1Task__ClosedLoop.html#a438826f3dbcafe0120feee9febee0731", null ],
    [ "next_time", "classtask__closedloop_1_1Task__ClosedLoop.html#ab04424c2ce17cc5faeee61c4ef633b32", null ],
    [ "period", "classtask__closedloop_1_1Task__ClosedLoop.html#a77298c5c9511e658d865f0c1419c622c", null ],
    [ "ref_speed", "classtask__closedloop_1_1Task__ClosedLoop.html#aac258aea75850295794669338e19a322", null ],
    [ "ref_speed_flag", "classtask__closedloop_1_1Task__ClosedLoop.html#a2397845dbf8e6c59f89660fdbc4b2abc", null ],
    [ "runs", "classtask__closedloop_1_1Task__ClosedLoop.html#af6079b14e7a74f3e5fb41d0b7656fec6", null ],
    [ "state", "classtask__closedloop_1_1Task__ClosedLoop.html#afe4fe832c014ec9f675d2dc5df058445", null ]
];