var annotated_dup =
[
    [ "BNO055", null, [
      [ "BNO055", "classBNO055_1_1BNO055.html", "classBNO055_1_1BNO055" ]
    ] ],
    [ "BNO055_1", null, [
      [ "BNO055", "classBNO055__1_1_1BNO055.html", "classBNO055__1_1_1BNO055" ]
    ] ],
    [ "closedloop", "namespaceclosedloop.html", [
      [ "ClosedLoop", "classclosedloop_1_1ClosedLoop.html", "classclosedloop_1_1ClosedLoop" ]
    ] ],
    [ "closedloop1", "namespaceclosedloop1.html", [
      [ "ClosedLoop", "classclosedloop1_1_1ClosedLoop.html", "classclosedloop1_1_1ClosedLoop" ]
    ] ],
    [ "DRV8847", "namespaceDRV8847.html", [
      [ "DRV8847", "classDRV8847_1_1DRV8847.html", "classDRV8847_1_1DRV8847" ],
      [ "Motor", "classDRV8847_1_1Motor.html", "classDRV8847_1_1Motor" ]
    ] ],
    [ "encoder", "namespaceencoder.html", [
      [ "Encoder", "classencoder_1_1Encoder.html", "classencoder_1_1Encoder" ]
    ] ],
    [ "motor_driver1", "namespacemotor__driver1.html", [
      [ "Motor", "classmotor__driver1_1_1Motor.html", "classmotor__driver1_1_1Motor" ]
    ] ],
    [ "shares", "namespaceshares.html", [
      [ "Queue", "classshares_1_1Queue.html", "classshares_1_1Queue" ],
      [ "Share", "classshares_1_1Share.html", "classshares_1_1Share" ]
    ] ],
    [ "task_closedloop", "namespacetask__closedloop.html", [
      [ "Task_ClosedLoop", "classtask__closedloop_1_1Task__ClosedLoop.html", "classtask__closedloop_1_1Task__ClosedLoop" ]
    ] ],
    [ "task_closedloop1", "namespacetask__closedloop1.html", [
      [ "Task_ClosedLoop", "classtask__closedloop1_1_1Task__ClosedLoop.html", "classtask__closedloop1_1_1Task__ClosedLoop" ]
    ] ],
    [ "task_encoder", "namespacetask__encoder.html", [
      [ "Task_Enc", "classtask__encoder_1_1Task__Enc.html", "classtask__encoder_1_1Task__Enc" ]
    ] ],
    [ "task_imu1", "namespacetask__imu1.html", [
      [ "Task_IMU", "classtask__imu1_1_1Task__IMU.html", "classtask__imu1_1_1Task__IMU" ]
    ] ],
    [ "task_motor", "namespacetask__motor.html", [
      [ "Task_Motor", "classtask__motor_1_1Task__Motor.html", "classtask__motor_1_1Task__Motor" ]
    ] ],
    [ "task_motor1", "namespacetask__motor1.html", [
      [ "Task_Motor", "classtask__motor1_1_1Task__Motor.html", "classtask__motor1_1_1Task__Motor" ]
    ] ],
    [ "task_touch1", "namespacetask__touch1.html", [
      [ "Task_Touch", "classtask__touch1_1_1Task__Touch.html", "classtask__touch1_1_1Task__Touch" ]
    ] ],
    [ "task_user", "namespacetask__user.html", [
      [ "Task_User", "classtask__user_1_1Task__User.html", "classtask__user_1_1Task__User" ]
    ] ],
    [ "task_user1", "namespacetask__user1.html", [
      [ "Task_User", "classtask__user1_1_1Task__User.html", "classtask__user1_1_1Task__User" ]
    ] ],
    [ "touch1", "namespacetouch1.html", [
      [ "Panel", "classtouch1_1_1Panel.html", "classtouch1_1_1Panel" ]
    ] ]
];